package day08todos;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

class Todo {

    public Todo(String task, Date dueDate, int hoursOfWork) {
        this.task = task;
        this.dueDate = dueDate;
        this.hoursOfWork = hoursOfWork;
    }

    private String task; // 2-50 characters long, must NOT contain a semicolon or | or ` (reverse single quote) characters
    private Date dueDate; // Date between year 1900 and 2100
    private int hoursOfWork; // 0 or greater number

    // format all fields of this Todo item for display exactly as specified below in the example interactions
    @Override
    public String toString() {
        return String.format("???");
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        if (task.length() < 2 || task.length() > 50) {
            throw new IllegalArgumentException("Task must be between 2-50 chars long");
        }
        this.task = task;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        // TODO: find a way to check year, suggestion use Calendar class
        this.dueDate = dueDate;
    }

    public int getHoursOfWork() {
        return hoursOfWork;
    }

    public void setHoursOfWork(int hoursOfWork) {
        if (hoursOfWork < 0) {
            throw new IllegalArgumentException("Hours of work can't be less than 0");
        }
        this.hoursOfWork = hoursOfWork;
    }

    final static SimpleDateFormat dateFormatFile = new SimpleDateFormat("todo");
    final static SimpleDateFormat dateFormatScreen = new SimpleDateFormat("todo");
}

public class Day08Todos {

    static int getMenuChoice() {
        System.out.print("Please make a choice [0-4]:\n"
                + "1. Add a todo\n"
                + "2. List all todos (numbered)\n"
                + "3. Delete a todo\n"
                + "4. Modify a todo\n"
                + "0. Exit\n"
                + "Your choice is: ");
        int choice = input.nextInt();
        input.nextLine();
        return choice;
    }

    static ArrayList<Todo> todoList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            int choice = getMenuChoice();
            switch (choice) {
                case 1:
                    addTodo();
                    break;
                case 2:
                    listAllTodos();
                    break;
                case 3:
                    deleteTodo();
                    break;
                case 4:
                    modifyTodo();
                    break;
                case 0:
                    System.out.println("Exiting. Good bye!");                    
                    return;
                default:
                    System.out.println("Invalid choice, try again.");
            }
            System.out.println();
        }
    }

    private static int addTodo() {
        try {
            System.out.println("Adding a todo.");
            System.out.print("Enter task description: ");
            String task = input.nextLine();
            System.out.print("Enter due Date (yyyy/mm/dd): ");
            String dueDateStr = input.nextLine();
            Date dueDate = Todo.dateFormatScreen.parse(dueDateStr);
            System.out.print("Enter hours of work (integer): ");
            int hours = input.nextInt();
            Todo todo = new Todo(task, dueDate, hours);
            todoList.add(todo);
            System.out.println("You've created the following todo:");
            System.out.println(todo);
        } catch () {

        }
    }

    private static void listAllTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void deleteTodo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void modifyTodo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
