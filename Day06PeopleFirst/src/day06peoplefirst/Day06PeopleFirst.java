package day06peoplefirst;

import java.io.File;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Scanner;

class Person {

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void print() {
        System.out.printf("%s is %d y/o\n", name, age);
    }

    String name;
    int age;
}

public class Day06PeopleFirst {

    static ArrayList<Person> peopleList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
        // peopleList.add(new Person("Jerry", 33));
        // peopleList.add(new Person("Maria", 22));
        // peopleList.add(new Person("Timmy", 53));
        try ( Scanner fileInput = new Scanner(new File("people.txt"))) {
            while (fileInput.hasNextLine()) {
                try {
                    String line = fileInput.nextLine();
                    String[] data = line.split(";"); // check invalid num of fields
                    if (data.length != 2) {
                        throw new InvalidParameterException("Error: invalid number of fields in line, skipping: " + line);
                    }
                    String name = data[0];
                    if (name.isEmpty()) {
                        throw new InvalidParameterException("Error: name must not be empty, skipping: " + line);
                    }
                    String ageStr = data[1];
                    int age = Integer.parseInt(ageStr); // parsing exception
                    if (age < 0 || age > 150) {
                        throw new InvalidParameterException("Error: age must be between 0-150, skipping: " + line);
                    }
                    Person p = new Person(name, age);
                    peopleList.add(p);                    
                } catch (NumberFormatException | InvalidParameterException ex) {
                    System.out.println("Error parsing value: " + ex.getMessage());
                    // continue;
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
            return; // or System.exit(1); because it is a fatal error
        }
        if (peopleList.isEmpty()) {
            System.out.println("List empty, nothing to do. exiting.");
            return;
        }
        for (Person p : peopleList) {
            p.print();
        }
        //
        Person youngestPerson = peopleList.get(0);
        Person oldestPerson = peopleList.get(0);
        for (Person p : peopleList) {
            if (p.age < youngestPerson.age) {
                youngestPerson = p;
            }
            if (p.age > oldestPerson.age) {
                oldestPerson = p;
            }
        }
        System.out.print("Youngest person: ");
        youngestPerson.print();
        System.out.print("Oldest person: ");
        oldestPerson.print();
        // Average age is: 41.50
        int ageSum = 0;
        for (Person p : peopleList) {
            ageSum += p.age;
        }
        double ageAvg = (double)ageSum / peopleList.size();
        System.out.printf("Average age is: %.2f\n", ageAvg);
        // Person with shortest name is: Mo 27 y/o
        Person shortestNamePerson = peopleList.get(0);
        for (Person p : peopleList) {
            if (p.name.length() < shortestNamePerson.name.length()) {
                shortestNamePerson = p;
            }
        }
        System.out.print("Person wiht shortest name is: ");
        shortestNamePerson.print();
        //
        // Person with matching name: Marianna 55 y/o
        // Person with matching name: Lillian 51 y/o
        System.out.print("Enter search string for names: ");
        String searchStr = input.nextLine();
        for (Person p : peopleList) {
            if (p.name.contains(searchStr)) {
                System.out.print("Person with matchin name: ");
                p.print();
            }
        }

    }

}
