package day04randoms;

import java.util.Random;
import java.util.Scanner;

public class Day04Randoms {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter minumum: ");
        int min = input.nextInt();
        System.out.print("Enter maximum: ");
        int max = input.nextInt();
        
        if (min < 0 || max < 0 || min > max) {
            System.out.println("Error: minimum must be less or equal to maximum, both positive integers.");
            System.exit(1); // return;
        }
        
        Random rand = new Random();
        for (int i = 0; i < 10; i++) {
            // int num = (int)(100 * Math.random()) + 1;
            int num = rand.nextInt(max-min+1) + min;
            System.out.println(num);
        }
        
    }
    
}
