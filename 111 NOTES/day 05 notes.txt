DAY 05 NOTES - PRINTF

About printf use
----------------

System.out.printf("format string", param1, param2, param3, ...)

- the number of % signs in format string must match number of additional parameters
- % signs are placeholders that will be replaced with the values of parameters

Useful % sign types:
%s - string
%d - decimal meaning any integer type
%f - floating point (double or float)
%.4f - floating point with exactly 4 decimal points precision,
e.g. if a value is 12.1234897 it will be displayed as 12.1235

%10s - string that will take at least 10 characters, spaces added in front, note that string will NOT be cut if it is longer than 10 characters.
System.out.printf("Name is %10s, age is...?", Jerry) will display
Name is      Jerry, age is ...?

%-10s - string that will take at least 10 characters, spaces added at the end
Name is Jerry     , age is ...?

System.out.printf("|%5d, %-5d|", 65, 123) will display:
|   65, 123  |

%10.4f - floating point with exactly 4 decimal points precision, 10 chars total
System.out.printf("|%10.4f|", 23.8273648712)
|   23.8274|

System.out.printf("|%05d|", 65) will display with "leading zeros":
|00065|


String.format() takes exactly the same parameters and works in exactly the same way except instead of printing the result it returns a String containing it.

String resultText = String.format("%s is %d y/o", Jerry, 33);

Will assing to resultText string "Jerry is 33 y/o".
It will not print out anything.

