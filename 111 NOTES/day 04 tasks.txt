DAY 04 TASKS

TASK 1 - Day04Randoms

Ask user for two positive integer numbers, minimum and maximum.
Make sure minimum is not larger than maximum.
If it is then display error and stop the program.

If all is good then generate 10 random integer numbers between minimum (inclusive) and maximum (inclusive).
Display the number one per line on the screen.

Example session:

Enter minumum: 5
Enter maximum: 3
Error: minimum must be less or equal to maximum.


Enter minumum: 5
Enter maximum: 13
4
7
9
13
4
5
5
...

TASK 2 - Day04Names

Ask user how many names they want to enter.
Ask user for the names, one per line.
Place each name in an array of strings.

Print out the names back to the user on a single line, comma-separated.

Example session:

How many names do you want to enter? 5
Enter name #1: Jerry
Enter name #2: Terry
Enter name #3: Barry
Enter name #4: Larry
Enter name #5: Marry

Your names were: Jerry, Terry, Barry, Larry, Marry





