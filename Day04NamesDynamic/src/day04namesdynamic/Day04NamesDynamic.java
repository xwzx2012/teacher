package day04namesdynamic;

import java.util.ArrayList;
import java.util.Scanner;

public class Day04NamesDynamic {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("How many names do you want to enter? ");
        int size = input.nextInt();
        input.nextLine(); // consume the left over newline
        ArrayList<String> namesList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            // System.out.print("Enter name " + (1+i) + "#: ");
            System.out.printf("Enter name %d#: ", i + 1);
            String name = input.nextLine();
            namesList.add(name);
        }
        System.out.print("Your names were: ");
        // String listStr = String.join(", ", namesArray);
        // System.out.println(listStr);
        for (int i = 0; i < namesList.size(); i++) {
            System.out.printf("%s%s", i == 0 ? "" : ", ", namesList.get(i));
        }
    }

}
