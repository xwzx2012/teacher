package day05arraycontains;

public class Day05ArrayContains {

    public static int[] concatenate(int[] a1, int[] a2) {
        int [] result = new int[a1.length + a2.length];
        for (int i = 0; i < a1.length; i++) {
            result[i] = a1[i];
        }
        for (int i = 0; i < a2.length; i++) {
            result[i + a1.length] = a2[i];
        }
        return result;
    }
    
    public static void printDups(int[] a1, int a2[]) {
        int [] doneArray = new int[a1.length];
        int doneCount = 0;
        
        for (int i = 0; i < a1.length; i++) {
            int v1 = a1[i];
            // check if it was printed out already
            boolean isAlreadyPrinted = false;
            for (int j = 0; j < doneCount; j++) {
                if (doneArray[j] == v1) { // already printed out                   
                    isAlreadyPrinted = true;
                    break;
                }
            }
            if (isAlreadyPrinted) {
                continue;
            }
            // check if it a duplicate to be printed
            for (int j = 0; j < a2.length; j++) {
                int v2 = a2[j];
                if (v1 == v2) { // found duplicate, print it
                    System.out.println(v1);
                    doneArray[doneCount] = v1;
                    doneCount++;
                    break;
                }
            }
        }
        System.out.println("done");
    }
    
    public static int[] removeDups(int[] a1, int[] a2) {
        int [] resultTooBig = new int[a1.length];
        int resultCount = 0;
        for (int i = 0; i < a1.length; i++) {
            int v1 = a1[i];
            boolean isDup = false;
            for (int j = 0; j < a2.length; j++) {
                int v2 = a2[j];
                if (v1 == v2) { // found duplicate
                    isDup = true;
                    break;
                }
            }
            if (!isDup) {
                resultTooBig[resultCount] = v1;
                resultCount++;
            }
        }
        // copy the elements that were stored to a probably smaller array
        int [] result = new int[resultCount];
        for (int i = 0; i < resultCount; i++) {
            result[i] = resultTooBig[i];
        }
        return result;
    }
    
    static void printArray(int[] array) {
        System.out.print("[");
        for (int i = 0; i < array.length; i++) {
            System.out.printf("%s%d", i==0 ? "": ", ", array[i]);
        }
        System.out.println("]");
    }
    
    public static void main(String[] args) {
        //printArray(concatenate(new int[]{1,2,3,4,5}, new int[]{6,7,8,9}));
        //printDups(new int[]{1, 3, 7, 8, 2, 7, 9, 11}, new int[]{3, 8, 7, 5, 7, 13, 5, 12});
        printArray(removeDups(new int[]{2, 3, 7, 9, 3, 0 }, new int[]{3, 7}));
    }
    
}
