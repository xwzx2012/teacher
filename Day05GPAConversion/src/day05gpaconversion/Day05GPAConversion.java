package day05gpaconversion;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Day05GPAConversion {

    static double gpaToNum(String gpaStr) {
        switch (gpaStr) {
            case "A":
                return 4.0;
            case "A-":
                return 3.7;
            case "B+":
                return 3.3;
            case "B":
                return 3;
            case "B-":
                return 2.7;
            case "C+":
                return 2.3;
            case "C":
                return 2;
            case "D":
                return 1;
            case "F":
                return 0;
            default:
                throw new IllegalArgumentException("GPA value invalid: " + gpaStr);
        }
    }

    static ArrayList<Double> gradesList = new ArrayList<>();

    public static void main(String[] args) {
        try ( Scanner fileInput = new Scanner(new File("grades.txt"))) {
            while (fileInput.hasNextLine()) {
                try {
                    String line = fileInput.nextLine();
                    double gpaVal = gpaToNum(line);
                    gradesList.add(gpaVal);
                } catch (IllegalArgumentException ex) {
                    System.out.println("Error parsing GPA value: " + ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
        //
        if (gradesList.isEmpty()) {
            System.out.println("No grades were read. Exiting.");
            return;
        }
        System.out.print("Grades List: ");
        for (int i = 0; i < gradesList.size(); i++) {
            System.out.printf("%s%.2f", i == 0 ? "" : ", ", gradesList.get(i));
        }
        System.out.println("");
        // average
        double sum = 0;
        for (int i = 0; i < gradesList.size(); i++) {
            sum += gradesList.get(i);
        }
        double avg = sum / gradesList.size();
        System.out.printf("Average is: %.2f\n", avg);
        // median
        Collections.sort(gradesList);
        double median;
        if (gradesList.size() % 2 == 1) {
            median = gradesList.get(gradesList.size()/2);
        } else {
            double v1 = gradesList.get(gradesList.size()/2 - 1);
            double v2 = gradesList.get(gradesList.size()/2);
            median = (v1 + v2)/2;
        }
        System.out.printf("Median is: %.2f\n", median);
        //
        double sumOfDIffSquares = 0;
        for (double val : gradesList) {
            sumOfDIffSquares += (val - avg)*(val - avg);
        }
        double stdDev = Math.sqrt(sumOfDIffSquares / gradesList.size());
        System.out.printf("Standard deviation: %.2f\n", stdDev);        
    }

}
